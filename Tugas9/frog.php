<?php
	require_once 'animal.php';

	/**
	 * Turunan kelas animal frog
	 */
	class Frog extends Animal
	{

		function jump()
		{
			return "Hop hop";
		}
	}
?>