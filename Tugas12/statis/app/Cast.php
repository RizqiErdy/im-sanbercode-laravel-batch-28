<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    protected $table = 'genre';
    protected $fillable =['nama','umur','bio'];
}
