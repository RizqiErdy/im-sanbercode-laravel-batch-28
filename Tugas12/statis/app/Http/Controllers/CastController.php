<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class CastController extends Controller
{
    public function create()
    {
    	return view('casts.create');
    }

    public function index()
    {
    	$select = DB::table('cast')->get();
    	return view('casts.index',compact("select"));
    }

    public function store(Request $request)
    {
    	$request->validate([
		    'nama' => 'required|unique:cast',
		    'umur' => 'required|max:2',
		    'bio' => 'required'
		]);

    	$query = DB::table('cast')->insert([
    		"nama" => $request["nama"],
    		"umur" => $request["umur"],
    		"bio" => $request["bio"],
    		
    	]);
    	return redirect('/casts')->with('success','Data Berhasil di Simpan!');
    }

    public function show($id)
    {
    	$post = DB::table('cast')->where('id',$id)->first();
    	return view('casts.show',compact("post"));
    }

    public function edit($id)
    {
        $query = DB::table('cast')->where('id',$id)->first();
        return view('casts.edit',compact("query"));
    }

     public function update($id, Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'umur' => 'required|max:2',
            'bio' => 'required'
        ]);

        $query = DB::table('cast')->where('id', $id)->update([
            "nama" => $request["nama"],
            "umur" => $request["umur"],
            "bio" => $request["bio"],
            
        ]);
        return redirect('/casts')->with('success','Data Berhasil di Simpan!');
    }

    public function destroy($id)
    {
    	$query = DB::table('cast')->where('id',$id)->delete();
    	return redirect('/casts')->with('success','Data Berhasil di Hapus!');
    }
}
