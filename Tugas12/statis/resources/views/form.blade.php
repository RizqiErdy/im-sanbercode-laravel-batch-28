<!DOCTYPE html>
<html>
	<head>
		<title>Form Pendaftaran</title>
	</head>
	<body>
		<h1>Buat Account Baru!</h1>
		<h2>Sign Up Form</h2>

		<form action="/welcome" method="POST">
			@csrf
			<label>First Name:</label><br><br>
			<input type="text" name="frname"><br><br>

			<label>Last Name:</label><br><br>
			<input type="text" name="lsname"><br><br>
			
			<label>Gender:</label><br><br>
			<input type="radio" name="gender" value="Male">Male<br>
			<input type="radio" name="gender" value="Female">Female<br>
			<input type="radio" name="gender" value="Other">Other<br><br>
			
			<label>Nationality:</label><br><br>
			<select name="nationality">
				<option value="idn">Indonesia</option>
				<option value="jpn">Jepang</option>
				<option value="ing">Inggris</option>
				<option value="mly">Malaysia</option>
			</select><br><br>

			<label>Language Spoken:</label><br><br>
			<input type="checkbox" name="bahasa" value="idn">Bahasa Indonesia<br>
			<input type="checkbox" name="bahasa" value="eng">English<br>
			<input type="checkbox" name="bahasa" value="oth">Other<br><br>

			<label>Bio:</label><br><br>
			<textarea name="bio" cols="30" rows="10"></textarea><br><br>

			<input type="submit" value="Sign Up">
		</form>
	</body>
</html>