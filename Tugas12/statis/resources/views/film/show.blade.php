@extends('adminlte.master')

@section('judul')
  Halaman FIlm {{$film->judul}}
@endsection
@section('content')

<div class="row mt-2 ml-2">
  <div class="col-3">
      <div class="card" style="width: 18rem;">
        <img class="card-img-top" src="{{asset('poster/'.$film->poster)}}" >
        <div class="card-body">
          <h5 class="card-title">{{$film->judul}} ({{$film->tahun}})</h5>
          <p class="card-text">{{$film->ringkasan}}</p>
        </div>
        <div class="card-body">
          <a class="btn btn-info" href="/film"></i>Kembali</a>
        </div>
      </div>
  </div>
</div>
    

@endsection