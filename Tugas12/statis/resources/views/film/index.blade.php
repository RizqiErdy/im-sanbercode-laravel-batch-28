@extends('adminlte.master')

@section('judul')
  Halaman FIlm
@endsection
@section('content')
<div class="card-body">
  @if(session('success'))
    <div class="alert alert-success">
      {{session('success')}}
    </div>
  @endif
  <a class="btn btn-primary mb-2" href="/film/create">Tambah</a>
  <div class="row mt-2 ml-2">

    @foreach($film as $item)
    <div class="col-3">
        <div class="card" style="width: 18rem;">
          <img class="card-img-top" src="{{asset('poster/'.$item->poster)}}" >
          <div class="card-body">
            <h5 class="card-title">{{$item->judul}} ({{$item->tahun}})</h5>
            <p class="card-text">{{ Str::limit($item->ringkasan, 30) }}</p>
          </div>
          <div class="card-body">
            <a class="btn btn-info" href="/film/{{$item->id}}"></i>Detail</a>
            <a class="btn btn-success" href="/film/{{$item->id}}/edit"></i>Edit</a>
            <form action="/film/{{$item->id}}" method="POST">
              @csrf
              @method('DELETE')
              <input type="submit" value="Hapus" class="btn btn-danger">
            </form>
          </div>
        </div>
    </div>
    @endforeach 
  </div>
</div>
    

@endsection