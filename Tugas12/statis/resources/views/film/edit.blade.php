@extends('adminlte.master')

@section('judul')
  Halaman Edit Film
@endsection
@section('content')

  <form role="form" action="/film/{{$item->id}}" method="POST">
    @csrf
    @method('PUT')
    <div class="card-body">
      <div class="form-group">
        <label for="judul">Judul</label>
        <input type="text" class="form-control" name="judul" placeholder="Masukkan Judul" value="{{old('judul',$item->judul)}}">
        @error('judul')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="tahun">Tahun</label>
        <input type="number" class="form-control" name="tahun" placeholder="Masukkan Tahun" value="{{old('tahun',$item->tahun)}}">
        @error('tahun')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="ringkasan">Ringkasan</label>
        <textarea class="form-control" rows="3" placeholder="Masukkan Ringkasan..." name="ringkasan">{{old('ringkasan',$item->ringkasan)}}</textarea>
        @error('ringkasan')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="genre">Genre</label>
        <select name="genre_id" class="form-control">
          <option value="{{$item->genre_id}}">{{$old_genre->nama}}</option>
          @foreach($genre as $item)
            <option value="{{$item->id}}">{{$item->nama}}</option>
          @endforeach
        </select>
        @error('genre_id')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      
    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
    
    <!-- /.content -->
@endsection