@extends('adminlte.master')

@section('judul')
  Halaman Form Cast
@endsection
@section('content')

  <form role="form" action="/casts" method="POST">
    @csrf
    <div class="card-body">
      <div class="form-group">
        <label for="nama">Nama</label>
        <input type="text" class="form-control" name="nama" placeholder="Masukkan Nama" value="{{old('nama','')}}">
        @error('nama')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="umur">umur</label>
        <input type="number" class="form-control" name="umur" placeholder="Masukkan Umur" value="{{old('umur','')}}">
        @error('umur')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <div class="form-group">
        <label for="bio">Bio</label>
        <textarea class="form-control" rows="3" placeholder="Masukkan Bio..." name="bio">{{old('bio','')}}</textarea>
        @error('bio')
          <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>

    </div>
    <!-- /.card-body -->

    <div class="card-footer">
      <button type="submit" class="btn btn-primary">Submit</button>
    </div>
  </form>
    
    <!-- /.content -->
@endsection