@extends('adminlte.master')

@section('judul')
  Halaman Cast
@endsection

@section('content')
  <div class="card-body">
    @if(session('success'))
      <div class="alert alert-success">
        {{session('success')}}
      </div>
    @endif
    <a class="btn btn-primary mb-2" href="/casts/create">Tambah</a>
    <table class="table table-bordered">
      <thead>                  
        <tr>
          <th style="width: 10px">#</th>
          <th>Nama</th>
          <th style="width: 40px">umur</th>
          <th>Bio</th>
          <th style="width: 200px">Action</th>
        </tr>
      </thead>
      <tbody>
        @forelse($select as $key => $post)
          <tr>
            <td>{{$key+1}}</td>
            <td>{{$post->nama}}</td>
            <td>{{$post->umur}}</td>
            <td>{{$post->bio}}</td>
            <td>
              <a class="btn btn-info" href="/casts/{{$post->id}}"><i class="fa fa-eye"></i>Detail</a>
              <a class="btn btn-success" href="/casts/{{$post->id}}/edit"><i class="fa fa-edit"></i>Edit</a>
              <form action="/casts/{{$post->id}}" method="POST">
                @csrf
                @method('DELETE')
                <input type="submit" value="Hapus" class="btn btn-danger">
              </form>  
            </td>
          </tr>
          @empty
          <tr>
              <td colspan='5' align='center'>Data Kosong</td>
          </tr>
        @endforelse
      </tbody>
    </table>
  </div>
@endsection