@extends('adminlte.master')

@section('judul')
  Halaman Detail Cast
@endsection

@section('content')
  <div class="card-body">
    <table class="table">
      <tr>
        <th>Nama</th>
        <td>:</td>
        <td>{{$post->nama}}</td>
      </tr>

      <tr>
        <th>Umur</th>
        <td>:</td>
        <td>{{$post->umur}}</td>
      </tr>
      <tr>
        <th>Bio</th>
        <td>:</td>
        <td>{{$post->bio}}</td>
      </tr>
      <tr>
        <th><a href="/casts" class="btn btn-info">Kembali</a></th>
      </tr>
    </table>
  </div>
@endsection